export function sum(a, b) {
    return a + b;
}
export function reverse(a, b) {
    const buffer = a;
    a = b;
    b = buffer;
    return [a, b];
}