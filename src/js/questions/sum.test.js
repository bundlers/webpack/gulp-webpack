import {sum, reverse} from "./sum.js";

test('adds 1 + 2 to equal 3', () => {
    expect(reverse(3, 2)).toEqual([2,1]);
});